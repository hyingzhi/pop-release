import pytest


@pytest.fixture(scope="session")
def hub(hub):
    for dyne in []:
        hub.pop.sub.add(dyne_name=dyne)

    hub.pop.config.load(
        ["pop_release"],
        cli="pop_release",
        parse_cli=False,
    )

    yield hub
